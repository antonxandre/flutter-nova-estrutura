import 'package:flutter/material.dart';
import 'package:rumoapp/screens/page_inicial/page.dart';
import 'package:rumoapp/screens/page_secundaria/page.dart';
import 'package:rumoapp/screens/terceira/page.dart';

class Router {
  static rotas() {
    return <String, WidgetBuilder>{
      '/inicio': (context) => PageInicial(
            titulo: "Pagina 1",
          ),
      '/segunda': (context) => PageSecundaria(path: "Desenho"),
      '/terceira': (context) => PageTereciaria(path: "Desenho"),
    };
  }
}
