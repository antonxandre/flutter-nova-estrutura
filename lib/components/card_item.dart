import 'package:flutter/material.dart';

class CardItem extends StatelessWidget {
  final String title, conteudo;
  final IconData icon;

  const CardItem({
    Key key,
    this.title,
    this.conteudo,
    this.icon,
  }) : super(key: key);
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(icon),
              ),
              Column(
                children: <Widget>[
                  Text("$title"),
                  SizedBox(height: 20),
                  Text("$conteudo"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
