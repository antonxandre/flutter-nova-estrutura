import 'package:flutter/material.dart';
import 'package:rumoapp/models/Peca.dart';
import 'package:rumoapp/services/Abstract_Service.dart';

class PageInicialController extends AbstractService<Peca> {
  PageInicialController({@required String path}) : super(path) {
    this.findAll();
  }

  @override
  Peca fromJson(json) => Peca.fromJson(json);
}
