import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:rumoapp/components/card_item.dart';

import 'controller.dart';

class PageSecundaria extends StatelessWidget {
  final String path;
  const PageSecundaria({
    Key key,
    @required this.path,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PageSecundariaController(path: path),
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_right),
              onPressed: () {
                Navigator.pushNamed(context, '/terceira');
              },
            ),
          ],
        ),
        body: Consumer<PageSecundariaController>(
          builder: (_, controller, __) {
            if (controller.items != null) {
              return ListView.builder(
                  itemCount: controller.items.length,
                  itemBuilder: (context, index) {
                    return CardItem(
                      title: controller.items[index].descricao,
                      conteudo: controller.items[index].obs,
                      icon: Icons.add_photo_alternate,
                    );
                  });
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }
}
