// To parse this JSON data, do
//
//     final peca = pecaFromJson(jsonString);

import 'dart:convert';

Peca pecaFromJson(String str) => Peca.fromJson(json.decode(str));

String pecaToJson(Peca data) => json.encode(data.toJson());

class Peca {
    Peca({
        this.id,
        this.peca,
        this.descricao,
        this.quantidade,
        this.catalogoId,
        this.desenhoId,
        this.item,
    });

    int id;
    String peca;
    String descricao;
    String quantidade;
    int catalogoId;
    int desenhoId;
    String item;

    factory Peca.fromJson(Map<String, dynamic> json) => Peca(
        id: json["id"],
        peca: json["peca"],
        descricao: json["descricao"],
        quantidade: json["quantidade"],
        catalogoId: json["catalogo_id"],
        desenhoId: json["desenho_id"],
        item: json["item"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "peca": peca,
        "descricao": descricao,
        "quantidade": quantidade,
        "catalogo_id": catalogoId,
        "desenho_id": desenhoId,
        "item": item,
    };
}
