import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rumoapp/routes/router.dart';
import 'package:rumoapp/screens/page_inicial/page.dart';
import 'package:rumoapp/screens/page_inicial/controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PageInicialController>(
          create: (context) => PageInicialController(path: 'Peca'),
        ),
      ],
      child: MaterialApp(
        title: 'Rumo App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: PageInicial(
          titulo: "Pagina 1",
        ),
        routes: Router.rotas(),
      ),
    );
  }
}
